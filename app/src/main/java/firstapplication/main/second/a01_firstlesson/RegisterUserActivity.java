package firstapplication.main.second.a01_firstlesson;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.List;

public class RegisterUserActivity extends AppCompatActivity {

    EditText textName;
    EditText textAge;
    RadioGroup radioGroupSex;
    RadioButton radioMale;
    RadioButton radioFemale;
    EditText textEmail;
    Button buttonSendEmail;
    EditText textPhone;
    Button buttonCall;
    EditText textAlternativeEmail;
    Button buttonRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        textName = findViewById(R.id.textName);
        textAge = findViewById(R.id.textAge);
        radioGroupSex = findViewById(R.id.radioGroupSex);
        radioMale = findViewById(R.id.radioMale);
        radioFemale = findViewById(R.id.radioFemale);
        textEmail = findViewById(R.id.textEmail);
        buttonSendEmail = findViewById(R.id.buttonSendEmail);
        textPhone = findViewById(R.id.textPhoneNumber);
        buttonCall = findViewById(R.id.buttonCall);
        textAlternativeEmail = findViewById(R.id.textAlternativeEmail);
        buttonRegister = findViewById(R.id.buttonRegister);

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // in order to let user proceed without entering all this information
                //if (validateData()) {
                //    registerUser();
                //}
                registerUser();
            }
        });

        buttonSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAndSendEmail();
            }
        });

        buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAndCall();
            }
        });
    }

    private void tooltip(CharSequence text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    private boolean assertNotEmpty(EditText editText, CharSequence validationMessage) {

        if (TextUtils.isEmpty(editText.getText())) {
            tooltip(validationMessage);
            editText.requestFocus();
            return false;
        }

        return true;
    }

    private boolean assertSexChecked() {

        if (radioMale.isChecked() || radioFemale.isChecked()) {
            return true;
        }

        tooltip("Укажите свой пол");

        return false;
    }

    private boolean assertCorrectEmailAddress(EditText editText, CharSequence validationMessage) {
        CharSequence email = editText.getText();

        if (!TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true;
        }

        tooltip(validationMessage);
        editText.requestFocus();
        return false;
    }

    private boolean assertCorrectPhone(EditText editText, CharSequence validationMessage) {
        CharSequence phone = editText.getText();

        if (phone.length() == 10) {
            return true;
        }

        tooltip(validationMessage);
        return false;
    }

    // todo: where is the best place to create static classes in the project

    // todo: how to setup git, which files should be included and excluded

    private boolean validateEmail() {
        return assertNotEmpty(textEmail, "Укажите вашу почту")
                && assertCorrectEmailAddress(textEmail, "Не верная почта");
    }

    private boolean validatePhone() {
        return assertNotEmpty(textPhone, "Укажите ваш телефон")
                && assertCorrectPhone(textPhone, "Телефон должен состоять из 10 символов");
    }

    private boolean validateData() {
        return
                assertNotEmpty(textName, "Укажите ваше имя")
                && assertNotEmpty(textAge, "Укажите ваш возраст")
                && assertSexChecked()
                        && validateEmail()
                        && validatePhone()
                && assertNotEmpty(textAlternativeEmail, "Укажите резервную почту")
                && assertCorrectEmailAddress(textAlternativeEmail, "Не верная резервная почта")
                ;
    }

    private void validateAndSendEmail() {
        if (validateEmail()) {
            String email = textEmail.getText().toString();
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("text/plain"); // HTTP.PLAIN_TEXT_TYPE doesn't work
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {email});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Registration data");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Registration body " + textName.getText().toString());
            startActivityIfSafe(emailIntent);
        }
    }

    private void validateAndCall() {
        if (validatePhone()) {
            String phone = textPhone.getText().toString();
            Uri uri = Uri.parse("tel:" + phone);
            Intent dialIntent = new Intent(Intent.ACTION_DIAL, uri);
            startActivityIfSafe(dialIntent);
        }
    }

    private void startActivityIfSafe(Intent intent) {
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
        boolean isIntentSafe = activities.size() > 0;

        if (isIntentSafe) {
            Intent chooser = Intent.createChooser(intent, "Выберите приложение");
            startActivity(chooser);
        }
    }

    private void registerUser() {
        tooltip("Все поля заполнены можно продолжить регистрацию");
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
        finish();
    }
}
